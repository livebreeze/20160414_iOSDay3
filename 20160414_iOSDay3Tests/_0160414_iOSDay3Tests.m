//
//  _0160414_iOSDay3Tests.m
//  20160414_iOSDay3Tests
//
//  Created by ChenSean on 4/14/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface _0160414_iOSDay3Tests : XCTestCase

@end

@implementation _0160414_iOSDay3Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    ViewController *vc = [ViewController new];
    
    XCTAssertTrue([vc accumulate:10] == 55, "accumulate() error");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
