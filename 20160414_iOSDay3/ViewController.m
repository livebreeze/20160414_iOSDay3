//
//  ViewController.m
//  20160414_iOSDay3
//
//  Created by ChenSean on 4/14/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *myText;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@end

@implementation ViewController
- (IBAction)onClick:(id)sender {
    NSString *text = self.myText.text;
    int inputNumber = [text intValue];
    
    int ans = [self accumulate: inputNumber];
    NSLog(@"%d", ans);
    
    NSString *result = [NSString stringWithFormat: @"%d", ans];
    self.myLabel.text = result;
}

- (int)accumulate: (int)n {
    int sum = 0;
    
    for (int i = 0; i <=n; i++) {
        sum += i;
    }
    
    return sum;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
