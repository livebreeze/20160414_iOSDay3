//
//  ViewController.h
//  20160414_iOSDay3
//
//  Created by ChenSean on 4/14/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (int)accumulate: (int)n;

@end

